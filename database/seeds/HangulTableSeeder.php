<?php

use Illuminate\Database\Seeder;
/**
 * Clase que puebla de datos la tabla hangul, en esta tabla se encuentran
 *  las letras en coreano, traducción y descripción de la misma.
 */
class HangulTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contents = Storage ::disk('hangul')->get('vowels.csv');//primero las vocales
        $arrayLines = explode(PHP_EOL, $contents);
        $columns = array();

        for ($i=0; $i < count($arrayLines)-1; $i++) {
            if($i == 0)
            {
                $columns = explode(",",$arrayLines[$i]);
            }else{
                $contentLine = explode(",",$arrayLines[$i]);
                $insertData = array();
                for ($o=0; $o < count($columns); $o++) {
                    try {
                        $insertData[$columns[$o]] = ($contentLine[$o] === "true") ? true :$contentLine[$o];
                    } catch (\QueryException $th) {
                    }
                }
                DB::table('hangul')->insert($insertData);
            }
        }

        //ahora las consonantes
        $contents = Storage ::disk('hangul')->get('consonants.csv');
        $arrayLines = explode(PHP_EOL, $contents);
        for ($i=0; $i < count($arrayLines)-1; ++$i) {
            if($i == 0)
            {
            }else{
                $contentLine = explode(",",$arrayLines[$i]);
                $insertData = array();
                for ($o=0; $o < count($columns); ++$o) {
                    try {
                        $insertData[$columns[$o]] = ($contentLine[$o] === "false") ? false :$contentLine[$o];
                    } catch (\Exception $th) {
                        //throw $th;
                    }
                }
                DB::table('hangul')->insert($insertData);
            }

        }
    }
}

<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('professors')->insert([
            'user_id' =>2,
            'classroom' => 'Hangul-happy',
            'presentation' => 'Ipso Lorem',
        ]);
        DB::table('students')->insert([
            'user_id' => 1,
            'professor_id'=> 1,
            'final_score' => 0,
        ]);
        DB::table('students')->insert([
            'user_id' => 4,
            'professor_id'=> 1,
            'final_score' => 0,
        ]);
    }
}

<?php

use App\Classes\RoleEnum;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Ejecuta e runner que pobla la base de datos con usuarios
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Quim',
            'surname'=> 'Quow',
            'email' => 'quom@gmail.con',
            'password' => bcrypt('password'),
            'role' => RoleEnum::$USER
        ]);

        DB::table('users')->insert([
            'name' => 'Maria',
            'surname'=> 'Leal',
            'email' => 'leal@gmail.con',
            'password' => bcrypt('password'),
            'role' => RoleEnum::$PROFESSOR
        ]);

        DB::table('users')->insert([
            'name' => 'Arnaldo',
            'surname'=> 'Quesada',
            'email' => 'quesada@gmail.con',
            'password' => bcrypt('password'),
            'role' => RoleEnum::$ADMIN
        ]);
        DB::table('users')->insert([
            'name' => 'val',
            'surname' => 'valval',
            'email' => 'vale@gmail.com',
            'role' => 'U',
            'active' => 1,
            'password' => bcrypt('Asdf1234!'),
            'created_at' => '2019-11-05 17:29:30',
            'updated_at' => '2019-11-05 17:29:30'
        ]);
    }

}

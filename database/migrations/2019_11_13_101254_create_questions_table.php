<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->timestamps();
            $table->unsignedBigInteger('option_1');
            $table->unsignedBigInteger('option_2');
            $table->unsignedBigInteger('option_3');

            $table->foreign('option_1')->references('id')->on('hangul');
            $table->foreign('option_2')->references('id')->on('hangul');
            $table->foreign('option_3')->references('id')->on('hangul');
            $table->integer('correct');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}

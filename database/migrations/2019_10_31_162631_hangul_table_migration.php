<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
/**
 * Archivo con la estructura de la tabla que contiene las letras coreanas
 */
class HangulTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hangul', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('letter')->unique();
            $table->string('translation',6);
            $table->boolean('is_vowel');
            $table->string('description',140);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hangul');
    }
}

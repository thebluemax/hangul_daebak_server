<?php

use App\Classes\RoleEnum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/**
 *  Fallback route, donde cae los que no tiene sitio.
 */
Route::fallback(function () {
    return response()->json(['message' => 'Not Found in server.'], 404);
})->name('api.fallback.404');

/**
 * Autorización
 *
 * */

Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');
Route::get('logout', 'Api\AuthController@logout')->middleware('auth:api');

/**
 *
 *     Recuperaciión de contraseña
 *
 */
Route::get('lostpass/{email}', 'Api\PasswordController@lostPass');
Route::post('profile/newpass', 'Api\ProfileController@changePassword');

/**
 * profile
 */
Route::get('profile', 'Api\ProfileController@index');
Route::post('profile', 'Api\ProfileController@update');
Route::post('profile/cancel', 'Api\ProfileController@cancel');

/**
 * Professors
 */
Route::get('professors', 'Api\ProfessorController@index');
Route::post('professors', 'Api\ProfessorController@update');
Route::get('professors/classroom', 'Api\ProfessorController@classroom');
Route::get('professors/{id}', 'Api\ProfessorController@show');
//Route::delete('professors/{id}', 'Api\ProfessorController@delete');

Route::get('noAuth', function () {
    return response()->json(['error' => 'Unauthorized'], 401);
});
/**
 *
 *  Peticiones para los usuarios
 */
//Route::apiResource('users', 'Api\UserController');
Route::get('users', 'Api\UserController@index');
Route::get('users/{id}', 'Api\UserController@show');
Route::post('users', 'Api\UserController@store');
Route::post('users/{id}', 'Api\UserController@update');
Route::delete('users/{id}', 'Api\UserController@destroy');
//Route::get('users/cancel/{id}', 'Api\UserController@cancel');
Route::get('users/cancel/{id}', 'Api\UserController@cancel');


Route::get('users/cancel/{id}', 'Api\UserController@cancel');
Route::get('users/newpass/{id}', 'Api\PasswordController@sendNewPassword');
/**
 * Peticiones para los estudiantes
 */
Route::get('students/', 'StudentController@show');
Route::get('students/classroom', 'StudentController@classroom');
Route::get('students/history', 'StudentController@getHistoryGrade');
/**
 * hangul API
 */
Route::get('hangul', 'HangulController@index');
Route::get('hangul/vowels', 'HangulController@vowels');
Route::get('hangul/consonants', 'HangulController@consonants');
/**
 * Classroom calls
 *
 */
Route::get('classrooms', 'Api\ClassroomController@index');
Route::post('classrooms/update', 'Api\ClassroomController@assignClassroom');
Route::get('classrooms/{professor_id}', 'Api\ClassroomController@classroomByProfessor');

/**
 * Api quiz y notas
 */
//Route::post('profile/debug', 'Api\ProfileController@captureReponse');
Route::post('quizzes/create', 'QuizController@create')->middleware('role:' . RoleEnum::$PROFESSOR);
Route::post('quizzes/save', 'QuizController@storeRandomQuiz')->middleware('role:' . RoleEnum::$USER);
Route::put('quizzes/save', 'QuizController@update')->middleware('role:' . RoleEnum::$USER);
Route::get('quizzes/{id}', 'QuizController@show');

Route::get('gradebooks/{student_id}', 'QuizController@indexStudentHistory');

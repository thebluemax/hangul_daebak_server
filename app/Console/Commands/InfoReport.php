<?php

namespace App\Console\Commands;

use App\Mail\SendReport;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
/**
 * Clase para generar un reporte de estadísticas de los usuarios del sistema.
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class InfoReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'info:students';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Confección y envío de informe de usuarios';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Enviando consulta.');

        $professors = DB::table('professors')->
        select('professors.id as id','professors.classroom as classroom','users.name as name', 'users.surname as surname')->
        leftJoin('users','users.id','=','professors.user_id')->get();
        $students = DB::table('students')->get();

        $numStudents = count($students);
        $num_professors = count($professors);

        $this->info("Procesando datos");

        //Array con los datos para injectarlos al template
        $this->info("Inicio variables");

        $infoReport = [];
        $infoReport ['title'] = 'Informe d\'activitats dels estudiants';
        $infoReport ['urlMailLogo']= asset('storage/korean.png');

        $infoReport ['total_students'] = $numStudents;
        $infoReport ['total_professors'] = $num_professors;;
        $infoReport ['promedi_estu_prof'] = $numStudents / $num_professors;

        $infoReport ['classroom'] = [];
        $infoReport ['professor'] = [];
        $infoReport ['numStudents'] = [];
        $infoReport['totalPoints'] = 0;
        //today
        $infoReport['date'] = date('d-j-Y H:i:s',time());

        $flag = true;
        //realizamos cuentas de estudiantes y buscamos al mejor estudiante
        $this->info("Procesando datos del mejor estudiante");

        foreach ($students as $student) {
            $this->info("Procesando estudiante => ".$student->id);
            if ($flag) {
                $maxStudent = $student;
                $flag = false;
            }
            $infoReport['totalPoints'] = $infoReport['totalPoints'] + $student->final_score;
            if ($maxStudent->final_score < $student->final_score) {
                $maxStudent = $student;
            }

        }
        $this->info("Procesando información aulas");
        // calculamos las relaciones con el profesor
        foreach ($professors as $professor) {

            $fullName = $professor->name." ".$professor->surname;
            $classroom = $professor->classroom;
            $infoReport ['classroom'][] = $classroom ;
            $infoReport ['professor'][] = $fullName;
        //    $this->info($classroom." -- ".$fullName);
            $numStudent = 0 ;
            $this->info("Procesando información => ".$classroom);


            foreach ($students as $student) {

                if ($student->professor_id == $professor->id) {
                    $numStudent++;
                    $this->info("Procesando estudiante => ".$student->id);
                }
            }
            $infoReport ['numStudents'][] = $numStudent;


        }

        $user = DB::table('users')->where('id',$maxStudent->user_id)->get()->first();
        $infoReport['maxStudent']  = $user->name.' '.$user->surname;
        $infoReport['maxStudentPoints'] = $maxStudent->final_score;
        //$this->info('maX estudiant -- '.$maxStudent->id.' '.$maxStudent->final_score);

        $infoReport['promediPoints'] = $infoReport['totalPoints'] /  $numStudents;

        $this->info("Procesando promedios");

        $gradebooks = DB::table('gradebooks')->leftJoin('quizzes','quizzes.id','quiz_id')->get();
        $infoReport ['num_gradebooks'] = count($gradebooks);
        $infoReport ['promediGradebooks'] = number_format($infoReport ['num_gradebooks'] /  $numStudents,2 ,',',' ');
        $infoReport ['numUnsolvedGradebooks'] = 0;
        $infoReport ['num_solved_gradebooks'] = 0;
        $infoReport ['numRandomGradebooks'] = 0;
        foreach ($gradebooks as $gradebook ) {
            if ($gradebook->to_do == true) {
                $infoReport ['numUnsolvedGradebooks'] ++;
            }

            if($gradebook->professor_id == null)
                $infoReport ['numRandomGradebooks'] ++;

            if ($gradebook->to_do == false) {
                $infoReport ['num_solved_gradebooks'] ++;
            }

        }
      $this->info("Generando PDF");

        $pdf = PDF::loadView('pdf.studentInfo',$infoReport);

        $nameReportFile = date('j_m_Y',time()).'_EstudentReport.pdf';
        $this->info("Almacenando archivo => ".$nameReportFile);

        Storage::disk('hangulReports')->put($nameReportFile, $pdf->output());
        //Log::debug($pdf->output());
        $email_data['date'] = $infoReport['date'];
        $email_data['PDFoutput'] = $pdf;
        $email_data['PDFfilename'] = $nameReportFile;
        $admins = User::where('role','A')->get();
        $this->info("Enviando Correo");

        foreach ($admins as $admin) {

            if ($admin) {
                Mail::to($admin->email)->send(new SendReport($email_data));
            }
        }
        $this->info("Commando ejecutado");
    }

}

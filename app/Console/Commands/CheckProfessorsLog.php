<?php

namespace App\Console\Commands;

use App\Classes\RoleEnum;
use App\Mail\SendProfessorLog;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
/**
 * Clase para el control temporal de las actividades de los profesores.
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class CheckProfessorsLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'info:workclock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Control de login de los professores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Iniciando Control de profesores');
        $ok = true;
        $email_data['professors'] = [];
        $email_data['date'] = [];
        $day = 60 * 60 * 24;
        $dayLimit = 2 * $day;
        $this->info('Iniciando variables');
        $professors = User::where('role', RoleEnum::$PROFESSOR)->get();
        $this->info('realizando consulta');

        $today = time();
        foreach ($professors as $professor) {

            if ($professor) {
                $this->info('Profesor => '. $professor->id);

                $logs = DB::table('users_use')->select('id', 'user_id', 'action', 'created_at')
                    ->where('user_id', $professor->id)->get()->last();
                if ($logs != null) {

                    $lastLog = strtotime($logs->{'created_at'});

                    $timeBetwen = $today- $lastLog;
                    $infoReport = [];
                    if ($timeBetwen > $dayLimit) {
                        $this->info('Profesor pasó el limite => '. $professor->id);
                        $ok = false;

                        $days = floor($timeBetwen / $day);
                        $email_data['professors'][] = $professor->name . " " . $professor->surname;
                        $email_data['date'][] = $days;
                    }
                } else {
                    $this->info('incidencia al Profesor => '. $professor->id);
                    $ok = false;

                    $email_data['professors'][] = $professor->name . " " . $professor->surname;
                    $email_data['date'][] = 'never';
                }

            }

            //  Mail::to($admin->email)->send(new SendReport($email_data));
        }
        // fin loop profesor
        if (!$ok) {
            $this->info('Enviando informe');

            $admins = User::where('role', 'A')->get();
            foreach ($admins as $admin) {

                if ($admin) {
                    if (isset($email_data['date'])) {
                        # code...
                        Mail::to($admin->email)->send(new SendProfessorLog($email_data));
                    }
                }
            }
        }
        $this->info('Cerrando Comando');

    }
}

<?php

namespace App\Console\Commands;

use App\User;
use App\Mail\SendReport;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
/**
 * Clase que genera los informes de actividades de los profesores.
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class InfoReportProfessor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'info:professor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $infoReport = [];
        $infoReport ['urlMailLogo']= asset('storage/korean.png');
        $infoReport ['title']= 'Informació de les activitats del professorat';
        $infoReport ['professor']=[];
        $infoReport ['init']=[];
        $infoReport ['classroom']=[];
        $infoReport ['presentation']=[];
        $infoReport ['lastLogin']=[];
        $infoReport ['studentClassroom']=[];
        $infoReport ['studentsPoints']=[];
        $infoReport['totalQuizzes'] = [];
        $infoReport['lastQuiz'] = [];
        $infoReport['firstQuiz'] = [];
        $infoReport['QuizzesToDo'] = [];
        $infoReport['porcentQuiz'] = [];
        $infoReport['date'] = date('d-j-Y H:i:s',time());

        $professors = DB::table('professors')->
        select('professors.id as id','professors.classroom as classroom','professors.presentation as presentation','users.name as name','professors.user_id as user_id', 'users.surname as surname','users.created_at as created_at')->
        leftJoin('users','users.id','=','professors.user_id')->get();

        $this->info('Enviando consulta.');

        $infoReport ['total_professors'] = $professors->count();

        $this->info('Evaluando consulta.');

        foreach ($professors as $professor) {

            $infoReport ['professor'][] =
            $professor->name.' '.$professor->surname;
            $infoReport ['init'][] = date('d/m/y',strtotime($professor->created_at));
            $infoReport ['classroom'][] = $professor->classroom;
            $infoReport ['presentation'][] =$professor->presentation;
            $this->info('Solicitando datos professor_id='.$professor->id.'.');
            $uses = DB::table('users_use')->where('user_id',$professor->user_id)->get()->last();

            if ($uses) {

                $infoReport ['lastLogin'][] = $uses->created_at != null?$uses->created_at : "2019-11-30 16:40:30";
            }else {
                $infoReport ['lastLogin'][] =  "2019-11-30 16:40:30";
            }
            $students = DB::table('students')->where('professor_id',$professor->id)->get();
            $infoReport ['studentClassroom'][] = $students->count();
            $total = 0;
            foreach ($students as $student) {
                $total += $student->final_score;
            }
            $infoReport ['studentsPoints'][]= $total;
            $this->info('Solicitando evaluaciones professor_id => '.$professor->id.'');

            $gradebooks = DB::table('gradebooks')->leftJoin('quizzes','quizzes.id','=','gradebooks.quiz_id')->where('quizzes.professor_id',$professor->id)->get();
            $professorStatics = [];

            $infoReport['totalQuizzes'] []= $gradebooks->count();
            $professorStatics['toDo'] = 0;
            $firstEntry = $gradebooks->first();
            $lastEntry = $gradebooks->last();
            foreach ($gradebooks as $gradebook) {

                if ($gradebook->to_do == 1) {
                    $professorStatics['toDo'] = $professorStatics['toDo'] + 1;
                }

            }
            $infoReport['QuizzesToDo'] [] = $professorStatics['toDo'];
            $day = 60 * 60 * 24;
            $hour = 60 * 60 ;
            $minuts = 60;

            if($firstEntry != null){
                $first = strtotime($firstEntry->created_at);
                $last = strtotime($lastEntry->created_at);
                $infoReport['firstQuiz'][] = date('d/m/y',strtotime($firstEntry->created_at));
                $infoReport['lastQuiz'][] =  date('d/m/y',strtotime($lastEntry->created_at));
                $dateRef = $last - $first;
                if ($day > $dateRef) {
                    $infoReport['porcentQuiz'][] =floor($dateRef / $day).' days';
                } else if ($hour > $dateRef){
                    $infoReport['porcentQuiz'][] = floor($dateRef / $hour).' hours';;
                }

            } else {
                $infoReport['firstQuiz'][] = "---";
                $infoReport['lastQuiz'][] = "---";
            }


            $this->info("Datos guardados");

        }

        $this->info('Creando el informe');
        $pdf = PDF::loadView('pdf.professor',$infoReport);

        $nameReportFile = date('j_m_Y',time()).'_ProfessorReport.pdf';
        Storage::disk('hangulReports')->put($nameReportFile, $pdf->output());
        $email_data['date'] = $infoReport['date'];
        $email_data['PDFoutput'] = $pdf;
        $email_data['PDFfilename'] = $nameReportFile;
        $admins = User::where('role','A')->get();
        $this->info('Enviando Informe');

        foreach ($admins as $admin) {

            if ($admin) {
                Mail::to($admin->email)->send(new SendReport($email_data));
            }
        }
        $this->info('Cerrando comando.');


    }
}

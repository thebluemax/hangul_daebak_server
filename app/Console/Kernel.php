<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
/**
 * Librería principal para el registro de los comandos y tareas programadas.
 *
 * @author Librería externa <laravel.com>
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\InfoReport',
        'App\Console\Commands\InfoReportProfessor',
        'App\Console\Commands\CheckProfessorslog',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('info:students') //->weekly()
                ->daily();
        $schedule->command('info:professor') //->monthly()
                ->daily()->withoutOverlapping();
        $schedule->command('info:workclock')
                ->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

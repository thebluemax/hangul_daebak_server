<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo de representación de una letra en coreano y sus datos asociados
 * para  la persistencia del mismo.
 */
class Hangul extends Model
{
    /**
     * Nombre de la tabla que representa el modelo
     *
     * @var string
     */
    protected $table = "hangul";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}

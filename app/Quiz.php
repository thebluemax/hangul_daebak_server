<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo de examene
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class Quiz extends Model
{
     /**
     * La tabla del modelo.
     *
     * @var string
     */
    protected $table = 'quizzes';
    //protected $primaryKey = null;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'professor_id'
    ];

     /**
     * La lista de preguntas que pertenecen al examen, agrega el valor answer de la tabla pivot a el modelo.
     *
     * @return Question[] La lista de preguntas
     */
    public function questions()
    {
        return $this->belongsToMany('App\Question','quiz_questions')
            ->using('App\QuizQuestion')->withPivot('answer');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Representación de una pregunta con opciones y respuesta correcta.
 *
 * @author Maximiliano Fernández<thebluemax13@gmail.com>
 */
class Question extends Model
{
    public $timestamps = false;
     /**
     * La tabla del modelo.
     *
     * @var string
     */
    protected $table = 'questions';
    //protected $primaryKey = null;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'option_1', 'option_2','option_3','correct'
    ];
    /**
     * Relación con el exámene que usa la pregunta
     *
     * @return Quiz El valro del quiz en la tabla Quizzes.
     */
    public function quizzes()
    {


        return $this->belongsToMany('App\Quiz')->using('App\QuizQuestion','quiz_questions')->withPivot('answer');
    }
    /**
     * Devuelve el modelo que representa un letra del Hangul coreano y primera opción del quiz.
     *
     * @return Hangul
     */
    public function hangul1()
    {

        return $this->belongsTo('App\Hangul','option_1');
    }
     /**
     * Devuelve el modelo que representa un letra del Hangul coreano y segunda opción del quiz.
     *
     * @return Hangul
     */
    public function hangul2()
    {

       // return $this->belongsTo('App\Hangul','option_1');
        return $this->belongsTo('App\Hangul','option_2');
      //  return $this->belongsTo('App\Hangul','option_3');
    }
     /**
     * Devuelve el modelo que representa un letra del Hangul coreano y tercera opción del quiz.
     *
     * @return Hangul
     */
    public function hangul3()
    {
       return $this->belongsTo('App\Hangul','option_3');
    }

}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
/**
 * Clase que controla los aspectos de creación y control de envío de mail de aviso de nuevo usuario.
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class NewUser extends Mailable
{
    use Queueable, SerializesModels;

    private $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */


    public function __construct($inputData)
    {
        $this->data = $inputData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New user')->view('email.newUser',$this->data);
    }
}

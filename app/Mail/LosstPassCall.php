<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
/**
 * Clase que controla los aspectos de creación y control de envío de mail, para la recuperación de contraseñas
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class LosstPassCall extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * array que contiene los datos de envío y cuerpo del mensaje.
     *
     * @var array
     */
    private $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */


    public function __construct($inputData)
    {
        $this->data = $inputData;
    }

    /**
     * Construye el mensaje, usa la vista email.passlost
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->data['subject'])->view('email.passLost', $this->data);
    }
}

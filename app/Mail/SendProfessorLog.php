<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
/**
 * Clase que controla los aspectos de creación y control de envío de mail, para los avisos de las actividades de los profesores
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class SendProfessorLog extends Mailable
{
    use Queueable, SerializesModels;

    private $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */


    public function __construct($inputData)
    {
        $this->data = $inputData;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Informe')
        ->view('email.infolog',$this->data);
    }
}

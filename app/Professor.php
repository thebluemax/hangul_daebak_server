<?php

namespace App;

use App\Http\Resources\User;
use Illuminate\Database\Eloquent\Model;
/**
 * Estructura para la persistencia del modelo de un Profesor
 *
 * @author Maximiliano Fernandez <thebluemax13@gmail.com>
 *
 */
class Professor extends Model
{
    /**
     * La tabla del modelo.
     *
     * @var string
     */
    protected $table = 'professors';
    //protected $primaryKey = null;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'classroom', 'presentation'
    ];
    /**
     * realación profesor usuario
     *
     * @return User el usuario del profesor.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

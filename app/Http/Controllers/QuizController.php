<?php

namespace App\Http\Controllers;

use App\Classes\HttpStatus;
use App\Classes\RoleEnum;
use App\Classes\StaticsUse;
use App\Gradebook;
use App\Mail\NewQuiz;
use App\Professor;
use App\Question;
use App\Quiz;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Controlador de los exámenes y gestor de registro de las notas del estudiante
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class QuizController extends Controller
{
    private $RANDOM_MESSAGE = 'Automatic Quiz';
    private $RANDOM_PROFESSOR = 0;

    private $statics;
    /***
     * inicia el sistema de autentificación
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->statics = new StaticsUse();
    }
    /**
     *  Obtener la historia académica de un estudiante.
     *
     *  @param int $id el id del estudiante
     * @return \Illuminate\Http\Response
     */
    public function indexStudentHistory($id)
    {
        $user = Auth::user();
        $student = Student::with('user')->find($id);
        $responseArray;
        if ($student || $id == 0) {
                switch ($user->role) {
                    case RoleEnum::$USER:
                        if ($student->user_id == $user->id) {
                            $gradebooks = Gradebook::where('student_id', $student->id)->get();
                            $responseArray = ['student' => $student, 'gradebooks' => $gradebooks];
                        } else {
                            return response()->json(['error', 'You only can see yours records'], HttpStatus::$FORBIDDEN);
                        }

                        break;
                        case RoleEnum::$PROFESSOR:
                            if ($id != 0) {
                                $gradebooks = Gradebook::where('student_id', $student->id)->get();
                                $responseArray = ['student' => $student, 'gradebooks' => $gradebooks];
                            } else {
                                $professor = Professor::where('user_id',$user->id)->get()->first();
                                $students = Student::with('user')->where('professor_id',$professor->id)->get();
                                $responseArray = [];
                                foreach ($students as $student) {
                                    $tempGradeBook = Gradebook::where('student_id',$student->id)->get();
                                    $tempEntry = array('student' => $student,
                                                        'nots' => $tempGradeBook
                                                    );
                                    $responseArray[]=$tempEntry;
                                }
                            }
                        break;
                            case RoleEnum::$ADMIN:
                                if ($id != 0) {
                                    $gradebooks = Gradebook::where('student_id', $student->id)->get();
                                    $responseArray = ['student' => $student, 'gradebooks' => $gradebooks];
                                } else {

                                    $students = Student::with('user')->all();
                                    $responseArray = [];
                                    foreach ($students as $student) {
                                        $tempGradeBook = Gradebook::where('student_id',$student->id)->get();
                                        $tempEntry = array('student' => $student,
                                                            'gradebook' => $tempGradeBook);
                                        $responseArray[]=$tempEntry;
                                    }
                                }

                        break;
                }
           // $gradebooks = Gradebook::where('student_id', $student->id)->get();
            return response()->json($responseArray, HttpStatus::$OK);
        } else {
            return response()->json(['error', 'Maybe you not are a student..'], HttpStatus::$FORBIDDEN);
        }
    }


    /**
     * El professor crea un quiz
     *@param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //$requestArray = $request->all();
        $requestArray = json_decode($request->getContent(), true);
        $data = json_decode($request->getContent(), true);
        Log::debug(Auth::user()->id);
        $professor = Professor::where('user_id',Auth::user()->id)->get()->first();

        Log::debug($data['quiz']);
        $requestArray = $requestArray['quiz'];
        DB::beginTransaction();
        $quiz = Quiz::create(array(
            'description' => $requestArray['description'],
            'professor_id' => $professor->id
        ));
        Log::debug($professor->id);

        if ($quiz) {
            $questions = [];
            for ($i = 0; $i < count($requestArray['questions']); $i++) {
                $questions[] = Question::create($requestArray['questions'][$i]);
            }
            $quiz->questions()->saveMany($questions);
            $student = Student::find($requestArray['student_id']);
            if ($student) {

                $gadeBookEntry = Gradebook::create(array(
                    'student_id' => $student->id,
                    'score' => 0,
                    'quiz_id' => $quiz->id
                ));
            }
            DB::commit();

            $this->statics->saveStatics(Auth::user()->id,StaticsUse::$ACTION_CREATE_TEST);
            $email_data = array(
                'user_name' => $student->user->name,
                'professor' => Auth::user()->name.' '.Auth::user()->surname,
                'description' => $requestArray['description']
            );
            Mail::to($student->user->email)->send(new NewQuiz($email_data));

            return response()->json([
                'quiz' => $quiz->id,
                'gradebook' => $gadeBookEntry->id
            ], HttpStatus::$OK);
        } else {
            DB::rollBack();


            return response()->json(['error' => 'Error can\'t save quiz'], HttpStatus::$SERVER_ERROR);
        }
    }

    /**
     * Guarda  el quiz resuelto por un estudiante
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRandomQuiz(Request $request)
    {

        $requestArray = $request->all();
        $data = json_decode($request->getContent(), true);


        Log::debug($data);
        $requestArray = $requestArray['quiz'];
        DB::beginTransaction();
        $quiz = Quiz::create(array(
            'description' => $this->RANDOM_MESSAGE,
            'professor_id' => $this->RANDOM_PROFESSOR
        ));

        if ($quiz) {
            $questions = [];
            for ($i = 0; $i < count($requestArray['questions']); $i++) {
                Log::debug($requestArray['questions'][$i]['answer']);
                $quest = Question::create($requestArray['questions'][$i]);

                $quiz->questions()->save($quest, array(
                    'answer' => $requestArray['questions'][$i]['answer']
                ));
                //  $quiz->pivot->answer = $requestArray['questions'][$i]['answer'];
                $quiz->update();
            }
            $student = Student::where('user_id',Auth::user()->id)->get()->first();
            $gadeBookEntry = Gradebook::create(array(
                'student_id' => $student->id,
                'score' => $requestArray['score'],
                'quiz_id' => $quiz->id,
                'to_do' => false
            ));
            if ($gadeBookEntry) {
                $student = Student::find($requestArray['student_id']);
                $student->final_score = $student->final_score + $requestArray['score'];
                $student->update();
            } else {
                DB::rollBack();
                Log::critical('student: ' . $requestArray['student_id'] . '; score: ' . $requestArray['score']);
                return response()->json(['error' => 'Error can\'t save score, ask admin.'], HttpStatus::$SERVER_ERROR);
            }
            DB::commit();
            return response()->json(['gradebook' => $gadeBookEntry->id], HttpStatus::$OK);
        } else {
            DB::rollBack();
            return response()->json(['error' => 'Error can\'t save quiz'], HttpStatus::$SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Int $id la id del quiz a recuperar.
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quiz = Quiz::with('questions.hangul1')->with('questions.hangul2')->with('questions.hangul3')->where('id', $id)->get()->first();

        if ($quiz) {
            $gradebook = Gradebook::where('quiz_id', $quiz->id)->get()->first();
            if ($gradebook) {
                $respostArray = array(
                    'id' => $quiz->id,
                    'description ' => $quiz->description,
                    'created_at' => $quiz->created_at,
                    'updated_at' => $quiz->updated_at,
                    'professor_id' => $quiz->professor_id
                );

                $respostArray['gradebook'] = $gradebook;
                $respostArray['questions'] = [];

                foreach ($quiz->questions as $question) {
                    $q = array(
                        'id' => $question->id,
                        'option_1' => $question->hangul1,
                        'option_2' => $question->hangul2,
                        'option_3' => $question->hangul3,
                        'correct' => $question->correct,
                    );
                    if ($gradebook->to_do == false) {
                        $q['answer'] = $question->pivot->answer;
                    }
                    $respostArray['questions'][] = $q;
                }

                return response()->json($respostArray, HttpStatus::$OK);
            } else {
                return response()->json(['error' => 'Don\'t found the entry in the gradebook'], HttpStatus::$NOT_FOUND);
            }
        } else {
            return response()->json(['error' => 'Don\'t found the quiz in database'], HttpStatus::$NOT_FOUND);
        }

        // $responseArray  = array('' => , );

    }

    /**
     * Show the form for editing the specified resource.
     *TODO que se va hacer con esto?
     * @param  \App\quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function edit(quiz $quiz)
    {
        //
    }

    /**
     * Actualiza un quiz con los datos que envia el estudiante.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $requestArray = json_decode($request->getContent(), true);
        Log::debug($requestArray['quiz']);
        $requestArray =  $requestArray['quiz'];
        $gradebook = Gradebook::find($requestArray['gradebook']);
        if ($gradebook) {
            DB::beginTransaction();
            $quiz = Quiz::with('questions')->find($gradebook->quiz_id);

            if ($quiz) {
                $arrayAnswers = $requestArray['questions'];
                $pointer = 0;
                foreach ($quiz->questions as $question) {

                    if ($question->id == $arrayAnswers[$pointer]['id']) {
                        //Log::debug($arrayAnswers[$pointer]['answer']);
                        $question->pivot->answer = $arrayAnswers[$pointer]['answer'];
                        $question->pivot->update();
                    } else {
                        DB::rollBack();
                        $msg = $arrayAnswers[$pointer]['id'] . ' <-req question != object in list -> ' . $question->id;
                        Log::error($msg);
                        return response()->json(['error' => 'Check your requeste :  ' . $msg . '  The question list is not correct'], HttpStatus::$BAD_REQUEST);

                    }
                    $pointer++;
                }
                $gradebook->score = $requestArray['score'];
                $gradebook->to_do = false;
                $gradebook->update();

                $student = Student::find($requestArray['student_id']);
                if ($student) {
                    if ($student->user_id == Auth::user()->id) {
                        $student->final_score = $student->final_score + $requestArray['score'];
                        $student->update();

                        DB::commit();

                        return response()->json(['student' => $student, 'gradebook' => $gradebook], HttpStatus::$OK);
                        # code...
                    } else {
                        Log::error('[ERROR] trampitas... user: '.Auth::user()->id .' - user test: '.$student->user_id);
                        return response()->json(['error' => 'Your user don\'t belong to the test user '], HttpStatus::$FORBIDDEN);
                    }

                } else {
                    DB::rollBack();
                    return response()->json(['error' => 'Don\'t found student in database'], HttpStatus::$NOT_FOUND);
                }
            } else {
                DB::rollBack();
                return response()->json(['error' => 'Don\'t found quiz in database'], HttpStatus::$NOT_FOUND);
            }
        } else {
            DB::rollBack();
            return response()->json(['error' => 'Don\'t found the entry in database'], HttpStatus::$NOT_FOUND);
        }
    }
}

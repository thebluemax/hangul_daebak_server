<?php

namespace App\Http\Controllers;

use App\Classes\HttpStatus;
use App\Classes\RoleEnum;
use App\Gradebook;
use Illuminate\Support\Facades\Auth;
use App\Professor;
use App\Student;
use Illuminate\Http\Request;

/**
 * Clase controladora del recurso Student, representación de un estudiante.
 */
class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('role:' . RoleEnum::$USER);
    }
    /**
     * Muestra alumno con su usuario y profesor.
     *
     * @param Integer $id Id del usuario en la base de datos.
     * @return Response $response Objeto Response.
     */
    public function show()
    {
        $user = Auth::user();
        if ($user) {
            $student = Student::with(['user', 'professor'])->where('user_id',$user->id)->get()->first();
            if ($student) {
                return response()->json( $student, HttpStatus::$OK);
            } else {
                return response()->json(array('error', 'Student not fund'), HttpStatus::$NOT_FOUND);
            }

        } else {
            return response()->json(array('error', 'You don\'t have the rigths'), HttpStatus::$FORBIDDEN);

        }


        if ($student) {


            return response()->json($student, HttpStatus::$OK);
        } else {
            return response()->json(array('status', 'Not user in database'), HttpStatus::$NOT_FOUND);
        }
    }

    public function classroom()
    {   $user = Auth::user();

        $student = Student::where('user_id', $user->id)->get()->first();
        $professor = Professor::find($student->professor->id);
        $students = Student::where('professor_id',$professor->id)->get();
        if ($students->count() > 0) {
            $studentsArray = [];
            foreach ($students as $student ) {
                $studentsArray [] = array (
                    'name' => $student->user->name,
                    'surname' => $student->user->surname,
                    'email' => $student->user->email,
                );
            }
            $responseData = array(
                'name' => $professor->classroom,
                'description' => $professor->presentation,
                'professor' => array(
                    'id' => $professor->id,
                    'name' => $professor->user->name,
                    'surname' => $professor->user->surname,
                    'email' => $professor->user->email,
                    'students'=> $studentsArray,
                )
            );
            return response()->json($responseData, HttpStatus::$OK);
        } else {
            return response()->json(array('status', 'Not students in this classroom'), HttpStatus::$NOT_FOUND);
        }
    }

    public function getHistoryGrade()
    {
        $user = Auth::user();
        if ($user) {
            $student = Student::where('user_id',$user->id)->get()->first();
        } else {
            return response()->json(['error' => 'You don\'t have access rigths'], HttpStatus::$UNAUTHORIZED);
        }

        if ($student) {
            $gradebooks = Gradebook::where('student_id', $student->id)->get();
            return response()->json($gradebooks, HttpStatus::$OK);

        } else {
            return response()->json(['error' => 'You don\'t have a student'], HttpStatus::$FORBIDDEN);
        }

        return response()->json($gradebooks, HttpStatus::$OK);
    }
}

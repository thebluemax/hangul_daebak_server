<?php

namespace App\Http\Controllers\Api;

use App\Classes\AssignClassroom;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Classes\StaticsUse;
use App\Classes\HttpStatus;
use App\Classes\RoleEnum;
use App\Mail\NewUser;
use App\Student;
use App\User;
use Illuminate\Support\Facades\Log;

/**
 *
 *  Controlador encargado del las acciones del login y registro de usuario.
 *
 *  @author Maximiliano fernandez <thebluemax13@gmail.com>
 *
 *
 */
class AuthController extends Controller
{
    /**
     * Instancia de StaticUse que guarda las estadísticas
     *
     * @var StaticsUse
     */
    private $statics;

    public function __construct()
    {
        $this->statics = new StaticsUse();
    }
    /**
     * Método para que un usuario se pueda registrar al sistema.
     *
     *
     * @param Request $request
     *                      objeto request con los datos de la petitición. Obligatorios name, surname password.
     * @return Request
     *                  Json con el token.
     */
    public function register(Request $request)
    {
        $email_data = [];
        //Validando entradas
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:150|min:3|string',
            'surname' => 'required|max:150|min:3|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], HttpStatus::$BAD_REQUEST);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        //$data_student = array();
        if ($user) {
            $student = Student::create(array(
                'user_id' => $user->id,
                'professor_id' => AssignClassroom::assign(),//temporal
                'final_score' => 0,
                )
            );

            $email_data['user_name'] = $user->name;

            if ($student) {
                Log::debug($student->professor->classroom);
                $email_data['classroom'] = $student->professor->classroom;

                $email_data['professor'] = $student->professor->user->name;

                $admins = User::where('role','A')->get();
                foreach ($admins as $admin) {

                    if ($admin) {
                        Mail::to($admin->email)->send(new NewUser($email_data));
                    }
                }
                $success['token'] = $user->createToken('register')->accessToken;
                return response()->json(['success' => $success], HttpStatus::$CREATED);
            } else {
                return response()->json(
                    array('error' => 'The user role was not created'),
                    HttpStatus::$SERVER_ERROR
                );
            }

        } else {
            return response()->json(
                array('error' => 'The user is not created'),
                HttpStatus::$SERVER_ERROR
            );
        }




    }
    /**
     *  Método responsable de comprobar correo, password y genera el token que da acceso al los recursos.
     *
     * @param Request $request
     *                  objeto request con los datos de la petitición. Obligatorios name, password.
     *
     * @return Request
     *              json con el token.
     */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], HttpStatus::$BAD_REQUEST);
        }
        Log::debug(request('email')."::".request('password'));
        Log::debug(Auth::attempt(['email' => request('email'), 'password' => request('password')]));
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('login')->accessToken; //generando el token
            $this->statics->saveStatics($user->id, "login"); //guardamos las estadísticas
            return response()->json(['success' => $success], HttpStatus::$OK);
        } else {
            $this->statics->saveStatics(0, "login-error"); //y si falla también
            return response()->json(['error' => 'Unauthorized'], HttpStatus::$UNAUTHORIZED);
        }
    }
    /**
     * Método de cierre de sesión
     *
     * @return Response
     *                  json
     */
    public function logout()
    {
        $user = Auth::user();
        if ($user) {
            //realizamos la consulta a la tabla que guarda la referencia al usuario.
            DB::delete('DELETE FROM oauth_access_tokens WHERE user_id = ?', [$user->id]);
            $this->statics->saveStatics($user->id, "logout");
            return response()->json(['success' => 'Close session'], HttpStatus::$OK);
        } else {
            return response()->json(['error' => 'Not user in database'], HttpStatus::$NOT_FOUND);
        }
    }
}

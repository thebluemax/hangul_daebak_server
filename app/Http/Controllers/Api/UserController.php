<?php

namespace App\Http\Controllers\Api;

use App\Classes\AssignClassroom;
use App\Classes\HttpStatus;
use App\Classes\RoleEnum;
use App\Http\Controllers\Controller;
use App\Professor;
use App\Student;
use App\User;
use ErrorException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\TryCatch;

/**
 *
 *  Controlador responsable de las acciones de gestión de datos de los usuarios.
 *
 *  @author Maximiliano Fernández <thebluemax13@gmail.com>
 *
 *
 */
class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('role:' . RoleEnum::$ADMIN);
    }
    /**
     * Método que devuelve los usuarios que existen a la base de datos.
     *
     *  @return Response  Array[] ;
     */
    public function index()
    {
        try {
            $users = User::all();

            if (count($users) > 0) {
                return response()->json($users, HttpStatus::$OK);
            } else {
                return response()->json(
                    array('error' => 'Empty request, no data found'),
                    HttpStatus::$NO_CONTENT
                );
            }
        } catch (ErrorException $ex) {

            return response()->json(
                array('error' => $ex->getMessage()),
                500
            );
        }
    }

    /**
     * Crear una cuenta de usuario. Es obligatorio agregar los parámetros name, surname, password. Si role no existe usa el rol por defecto. Este método crea el objeto del tipo de role
     *
     * @param Request $request
     *                          La petición con los datos.
     *
     * @return Response
     *                          json;
     *
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:150',
            'surname' => 'string|max:150',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['error' => $validator->errors()],
                HttpStatus::$BAD_REQUEST
            );
        }
        //Guardamos los datos
        try {
            $user = User::create(array(
                'name' => $request->input('name'),
                'surname' => $request->input('surname'),
                'email' => $request->input('email'),
                'password' =>  bcrypt($request->input('password')),
                'role' => $request->input('role') ? $request->input('role') : RoleEnum::$USER,
            ));
        } catch (QueryException $ex) {
            return response()->json(
                array('error' => $ex->getMessage()),
                HttpStatus::$BAD_REQUEST
            );
        }
        //Lo asociamos a el modelo de  rol
        $rol = null;
        if ($user) {
            switch ($user->role) {
                case RoleEnum::$PROFESSOR:
                        $rol = Professor::create(array(
                            'user_id' => $user->id,
                            'classroom' => "class-".Time(),
                            'presentation' => 'no data'
                        ));
                    break;

                case RoleEnum::$USER:
                $rol = Student::create(array(
                    'user_id' => $user->id,
                    'professor_id' => AssignClassroom::assign(),
                    'final_score' => 0,
                ));
                    break;
                    // de momento no hacemos nada con admin
            }

            if ($rol || $request->input('role') == RoleEnum::$ADMIN) {
                return response()->json($user, HttpStatus::$CREATED);
            } else {
                return response()->json(
                    array('error' => 'The associated rol is not created'),
                    HttpStatus::$BAD_REQUEST
                );
            }

           // return response()->json($user, HttpStatus::$CREATED);
        } else {
            return response()->json(
                array('error' => 'The user is not created'),
                HttpStatus::$BAD_REQUEST
            );
        }
    }

    /**
     * Recupera los datos de un usuario a la DB.
     *
     * @param integer $id
     *                  la id del usuario.
     *
     * @return Response
     *                  json;
     *
     */
    public function show($id)
    {
        $user = User::find($id);

        if ($user) {
            return response()->json($user, HttpStatus::$OK);
        } else {
            return response()->json(
                array('error' => 'Not user in database'),
                HttpStatus::$NOT_FOUND
            );
        }
    }

    /**
     * Actualizarlos datos de un usuario. Los parametros name, surname, y role son opcionales.
     * Solo cambiarán los campos incluidos en la petición.
     *
     *
     * @param Request $request
     *                      La petición con los datos.
     * @return Response
     *                      json;
     *
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:150',
            'surname' => 'string|max:150',
            'role' => 'string|min:1|max:3',
        ]);
        $logA = $request->all();
        $data = json_decode($request->getContent(), true);


        Log::debug($data);
        foreach ($logA as $logK => $logV) {


            Log::debug($logK.' => '.$logV);
        }
        if ($validator->fails()) {
            return response()->json(
                ['error' => $validator->errors()],
                HttpStatus::$BAD_REQUEST
            );
        }
        $user = User::find($id);

        if ($user) {
            try {
                if ($request->input('name'))
                    $user->name = $request->input('name');
                if ($request->input('surname'))
                    $user->surname = $request->input('surname');
                if ($request->input('role') && $request->input('role') != $user->role )
                    $user->role = $this->update_rol($request->input('role'),$user->id);

                $user->update(); //persistimos los cambios.
                //$user->refresh();
            } catch (QueryException $ex) {
                return response()->json(array('error' => $ex->getMessage()), 500);
            }
            return response()->json($user, HttpStatus::$OK);
        } else {
            return response()->json(array('error' => 'Not user in database'), HttpStatus::$NOT_FOUND);
        }
    }
    /**
     * Undocumented function
     *
     * @param [type] $rol
     * @param [type] $user_id
     * @return void
     */
    private function update_rol($rol, $user_id)
    {
        switch($rol)
        {

            case RoleEnum::$PROFESSOR:
                $isStudent = Student::where('user_id',$user_id)->first();
               // var_dump($isStudent->professor_id);
                if($isStudent){
                    $isStudent->delete();
                }
                    $prof = Professor::create(array(
                        'user_id' => $user_id,
                        'classroom' => "class-".Time(),
                        'presentation' => 'no data'
                    ));
                break;

            case RoleEnum::$USER:
            $isProfessor = Professor::where('user_id',$user_id)->first();

                if($isProfessor){
                    $isProfessor->delete();
                }
            $student = Student::create(array(
                'user_id' => $user_id,
                'professor_id' => AssignClassroom::assign(),
                'final_score' => 0,
            ));
                break;
                // de momento no hacemos nada con admin
        }
        return $rol;
    }

    /**
     * Eliminar una cuenta de usuario.
     *
     * @param int $id
     *                  la id del usuario.
     *
     * @return Response
     *                  json;
     *
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user) {

            $user->delete();

            return response()->json(array('success' => 'User deleted'),  HttpStatus::$OK);
        } else {
            return response()->json(array('error' => 'Not user in database'), HttpStatus::$NOT_FOUND);
        }
    }
    /**
     * Cancelar una cuenta de usuario.
     *
     * @param int $id
     *                  la id del usuario.
     *
     * @return Response
     *                  json;
     *
     */
    public function cancel($id)
    {
        $user = User::find($id);
        if ($user) {

            $user->active = false;
            $user->update();

            return response()->json(array('success' => 'User Canceled'),  HttpStatus::$OK);
        } else {
            return response()->json(array('error' => 'Not user in database'), HttpStatus::$NOT_FOUND);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Classes\HttpStatus;
use App\Classes\StaticsUse;
use App\Http\Controllers\Controller;
use App\Mail\LosstPassCall;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
/**
 * PasswordController, se encarga de la gestión de recuperación
 * de contraseñas
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 * @
 */
class PasswordController extends Controller
{
    private $statics;

    public function __construct()
    {
        $this->statics = new StaticsUse();
    }
    /**
     * Solicita una nueva contraseña y la envía a la cuenta proporcionada
     * si la cuenta existe en la base de datos.
     *
     * @param $email User email
     *
     * @return User object;
     */
    public function lostPass($email)
    {
        $user = User::where('email', $email)->first();
            $is_mail = preg_match('/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}/',$email);// si el mail es correcto

            if($is_mail != 1)
                return response()->json(array('error' => 'Is not a correct email.'), HttpStatus::$BAD_REQUEST);

        if ($user) {
            $newPass = substr(md5(time() . $user->name), 0, 9);
            $user->password =  bcrypt($newPass);
            $user->save();
            $for_mail = $user->email;//preparamos correo
            $data = array(
                'name' =>  $user->name,
                'subject' => 'Sol·licitut de nova contrasenya',
                'password' => $newPass,
            );

            Mail::to($for_mail)->send(new LosstPassCall($data));// enviamos el correo
            $this->statics->changePass($user,'user');//estadisticas i controll
            return response()->json(array('success'=> 'Sent to user'), HttpStatus::$OK);
        } else {
            return response()->json(array('error'=> 'Not user in database'), HttpStatus::$NOT_FOUND);
        }
    }

    /**
     * Cambia y envia un nuevo password a un usurio, según su id
     *
     * @param [int] $id
     * @return json Response response
     */
    public function sendNewPassword($id)
    {
        $user = User::find($id);

        if ($user) {
                $newPass = substr(md5(time().$user->name),0, 9);
                $user->password =  bcrypt($newPass);
                $user->save();
                $this->statics->changePass($user,'admin');
                $data = array(
                    'name' =>  $user->name,
                    'subject' => 'Sol·licitut de nova contrasenya',
                    'password' => $newPass,
                );
                Mail::to($user->email)->send(new LosstPassCall($data));
            return response()->json(array('status'=>'New pass send'),  HttpStatus::$OK)
            ;
        } else {
            return response()->json(array('error' => 'Not user in database'), HttpStatus::$NOT_FOUND);
        }
    }
}


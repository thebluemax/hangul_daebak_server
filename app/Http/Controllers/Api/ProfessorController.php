<?php

namespace App\Http\Controllers\Api;

use App\Classes\HttpStatus;
use App\Classes\RoleEnum;
use App\Http\Controllers\Controller;
use App\Professor;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


/**
 * Controlador de la gestión de los profesores
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class ProfessorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('role:' . RoleEnum::$PROFESSOR);
    }
    /**
     * Lista los Profesores de la base de datos
     *
     * @return $response Response Objeto Response.
     */
    public function index()
    {
        $professors = Professor::with('user')->get();

        if ($professors) {

            return response()->json($professors, HttpStatus::$OK);
        } else {
            return response()->json(['error'=> 'Not user in database'], HttpStatus::$NOT_FOUND);
        }
    }
    /**
     * Muestra un profesor en la base de datos.
     *
     * @param Integer $id Id del usuario en la base de datos.
     * @return Response $response Objeto Response.
     */
    public function show($id)
    {
        $professor = Professor::with('user')->find($id);

        if ($professor) {

            return response()->json($professor, HttpStatus::$OK);
        } else {
            return response()->json(array('error' => 'Not user in database..'), HttpStatus::$NOT_FOUND);
        }
    }
    /**
     * obtener la clase y sus alumnos
     *
     * @param [type] $id
     * @return void
     */
    public function classroom()
    {
        $user = Auth::user();
        $professor = Professor::with('user')->where('user_id',$user->id)->get()->first();

        if ($professor) {
            $students = Student::with('user')->get()->where('professor_id', $professor->id);

            if ($students->count() > 0) {
                $studentArray = [];
                foreach ($students as $student) {
                    $studentArray[] = $student;
                }
                $classroom = array(
                    'name' => $professor->classroom,
                    'description' => $professor->presentation,
                    'professor' => [
                        'id' => $professor->id,
                        'name' => $professor->user->name,
                        'surname' => $professor->user->surname,

                    ],
                    'students' => $studentArray
                );

                return response()->json($classroom, HttpStatus::$OK);
            } else {
                return response()->json(['error', 'Not students for this professor in database'], HttpStatus::$NOT_FOUND);
            }
        } else {
            return response()->json(['error', 'Not found in database'], HttpStatus::$NOT_FOUND);
        }
    }
    /**
     * Muestra un profesor en la base de datos.
     *
     * @param Integer $id Id del usuario en la base de datos.
     * @param Request $request Un ojeto request con los datos de la petición
     * @return Response $response Objeto Response.
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'classroom' => 'string|max:140',
            'presentation' => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], HttpStatus::$BAD_REQUEST);
        }
        $user=Auth::user();
        if ($user) {

            $professor = Professor::where('user_id',$user->id)->get()->first();
            if ($professor) {

                //  $professor->user_id = $request->input('user_id') ?  $request->input('user_id') : $professor->user_id;

                $professor->classroom = $request->input('classroom') ? $request->input('classroom') : $professor->classroom;

                $professor->presentation = $request->input('presentation') ? $request->input('presentation') : $professor->classroom;

                $professor->save();

                return response()->json($professor, HttpStatus::$OK);
            } else {
                return response()->json(array('error', 'Not professor in database'), HttpStatus::$NOT_FOUND);
            }
        } else {
            return response()->json(array('error', 'you don\'t have rigths'), HttpStatus::$UNAUTHORIZED);

        }
    }

}

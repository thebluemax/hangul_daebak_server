<?php

namespace App\Http\Controllers\Api;

use App\Classes\HttpStatus;
use App\Http\Controllers\Controller;
use App\Mail\LosstPassCall;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

/**
 * Gestión de los datos de perfil del usuario
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Método que devuelve los datos del usuario.
     *
     *  @return Response  User object;
     */
    public function index()
    {
        $user = Auth::user(); // el usuario del token
        if ($user && Auth::check()) {
            return response()->json(['success' => $user], HttpStatus::$OK);
        } else {
            return response()->json(['error' => 'Unauthorized'], HttpStatus::$UNAUTHORIZED);
        }
    }
    /**
     * Modifica los datos de un usuario
     *
     * @param Request $request la petición con los datos. Opcionales name y surname.
     *
     * @return Response  User object;
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:150',
            'surname' => 'string|max:150',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], HttpStatus::$BAD_REQUEST);
        }

        $input = $request->all();

        $user = Auth::user();
        if ($user && Auth::check()) {

            $user->name = $input['name'] ? $input['name'] : $user->name;
            $user->surname = $input['surname'] ? $input['surname'] : $user->surname;
            $user->save();
            return response()->json(['success' => $user], HttpStatus::$OK);
        } else {
            return response()->json(['error' => 'Unauthorized'], HttpStatus::$UNAUTHORIZED);
        }
    }
    /**
     * Cancelar la cuenta de usuario, debe usar el password para cancelar
     *
     * @param Request $request
     *                      la petición con los datos. Obligatorio password.
     *
     * @return Response
     *                      json;
     *
     */
    public function cancel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|max:200',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], HttpStatus::$BAD_REQUEST);
        }
        $user = Auth::user(); // el usuario activo.

        if ($user && Auth::check()) {
            $pass = $request->input('password');

            if (Hash::check($pass, $user->password)) {
                $user->active = false;
                $user->save();

                return response()->json(['success' => 'User canceled'], HttpStatus::$OK);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], HttpStatus::$UNAUTHORIZED);
        }
    }
    /**
     * Cambio de contraseña
     *
     * Request $request la petición con los datos. Obligatorio old_password y new_password.
     *
     * @return Response  json;
     */
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required|string|min:8|max:200',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], HttpStatus::$BAD_REQUEST);
        }
        $id = $request->input('id_user');
        $old_password = $request->input('old_password');
        $new_password = $request->input('new_password');
        $user = Auth::user();

        if ($user && Auth::check()) {

            if (Hash::check($old_password, Auth::user()->password)) {
                $user->password = bcrypt($new_password);
                $user->save();

                return response()->json(['success' => 'Password Change'], HttpStatus::$OK);
            } else {
                return response()->json(['error' => 'You don\'t have rigths for this operation'], HttpStatus::$FORBIDDEN);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], HttpStatus::$UNAUTHORIZED);
        }
    }
    public function captureReponse(Request $request)
    {
        $input = $request->all();
        Log::debug($input);

      //  $contents = Storage ::disk('hangul')->get('vowels.csv');//primero las vocales
    }
}

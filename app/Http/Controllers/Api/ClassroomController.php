<?php

namespace App\Http\Controllers\Api;

use App\Classes\HttpStatus;
use App\Classes\RoleEnum;
use App\Http\Controllers\Controller;
use App\Professor;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
/**
 * Controlador para la gestión de estudiantes i las classes asignadas
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class ClassroomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('role:' . RoleEnum::$ADMIN);
    }
    /**
     * Muestra todas las clases con  alumnos y profesor.
     * Si el profesor no tiene alumnos el listado de alumnos estará vacío
     *
     * @return Resonse json listado de aulas
     */
    public function index()
    {
        $professors = Professor::with('user')->get();
        $responseArray = [];
        $tempArray = [];
        if ($professors->count() > 0) {
            # code...
            foreach ($professors as $professor) {
                $students = Student::with('user')->where('professor_id', $professor->id)->get();
                $tempArray['classroom'] = $professor->classroom;
                $tempArray['presentation'] = $professor->presentation;
                $tempArray['professor'] = $professor;
                $tempArray['total_students'] = $students->count();
                $tempArray['students'] = $students;
                $responseArray[] = $tempArray;
            }
            return response()->json($responseArray, HttpStatus::$OK);
        } else {
            return response()->json(['error' => 'Not professors in database'], HttpStatus::$NO_CONTENT);
        }
    }
    /**
     * Obtener los alumnos y datos de un aula según el del id profesor
     *
     * @param [type] $id_professor
     * @return Respose json lista aula
     */
    public function classroomByProfessor($id_professor)
    {
        $professor = Professor::with('user')->find($id_professor);

        if ($professor) {
            $students = Student::with('user')->where('professor_id', $professor->id)->get();
            $classroom = [
                'professor' => $professor,
                'students' => $students
            ];
            return response()->json($classroom, HttpStatus::$OK);
        } else {
            return response()->json(['error' => 'Not exist the professor in database'], HttpStatus::$NO_CONTENT);
        }
    }
    /**
     * Asigna un aula a un alumno.
     *
     * @param Request $request professor_id id del professor , student_id id del usuario
     * @return Response json
     */
    public function assignClassroom(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'professor_id' => 'integer',
            'student_id' => 'integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], HttpStatus::$BAD_REQUEST);
        }

        $student = Student::find($request->input('student_id'));
        $professor = Professor::find($request->input('professor_id'));
        if ($professor && $student) {
            $student->professor_id = $professor->id;
            $student->update();
            return response()->json($student, HttpStatus::$OK);
        } else {
            return response()->json(['error' => 'Can\'t find student or professor in the database'], HttpStatus::$NOT_FOUND);
        }

    }
}

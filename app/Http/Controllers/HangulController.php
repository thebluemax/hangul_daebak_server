<?php

namespace App\Http\Controllers;

use App\Classes\HttpStatus;
use App\Hangul;
use Illuminate\Http\Request;
/**
 * Controlador para las letras y  el alfabeto coreanos
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class HangulController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Lista todas las letras y devuelve un array
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hangul = Hangul::all();
        if($hangul){

        return response()->json($hangul, HttpStatus::$OK);
        } else {
            return response()->json(array('error', 'Not data in database'), HttpStatus::$NOT_FOUND);
        }
    }

    /**
     * Lista todas las letras vocales y devuelve un array
     *
     * @return \Illuminate\Http\Response
     */
    public function vowels()
    {
        $hangulVowels = Hangul::get()->where('is_vowel',true);

        if($hangulVowels){

        return response()->json($hangulVowels, HttpStatus::$OK);
        } else {
            return response()->json(array('error', 'Not data in database'), HttpStatus::$NOT_FOUND);
        }
    }
/**
     * Lista todas las letras consonantes y devuelve un array
     *
     * @return \Illuminate\Http\Response
     */
    public function consonants()
    {
        $hangulCons = Hangul::where('is_vowel',false);
        //var_dump($hangulCons);
        if($hangulCons->count() > 0){
           // var_dump($hangulCons);
        return response()->json($hangulCons->get(), HttpStatus::$OK);
        } else {
            return response()->json(array('error', 'Not data in database'), HttpStatus::$NOT_FOUND);
        }
    }
}

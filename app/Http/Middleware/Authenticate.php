<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Route;
/**
 * Interface para la implementación del sistema de autenticación nativo.
 *
 * @author Librería Externa <laravel.com>
 */
class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
try{
        return  'api/noAuth';
    } catch (ErrorException $ex) {
        return $ex->getMessage();
    }
    }
}

<?php

namespace App\Http\Middleware;

use App\Classes\HttpStatus;
use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * clase middleware  responsable de verificar los permisos por los rol de los usuarios.
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param String $role
     *                  rol que el usuario debe tener para usar el recurso.
     * @return Response
     *                  json
     */
    public function handle($request, Closure $next, $role)
    {
        if ( $request->user()->role !== $role  ) {
           return  response()->json(array('error' => 'You don\'t have the access rights.'), HttpStatus::$FORBIDDEN);
        }

        return $next($request);
    }
}

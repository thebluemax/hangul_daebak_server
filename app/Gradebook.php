<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Modelo de la representación de un libro de calificaciones, cada entrada es un calificación de un examen resuelto.
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class Gradebook extends Model
{
     /**
     * La tabla del modelo.
     *
     * @var string
     */
    protected $table = 'gradebooks';
    //protected $primaryKey = null;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id', 'score','quiz_id', 'to_do'
    ];
    /**
     * Relación notas estudiante
     *
     * @return Student El estudiante que pertenece el examen.
     */
    public function student()
    {
        return $this->belongsTo('App\Student');
    }
     /**
     * Relación nota examen
     *
     * @return Quiz El examen.
     */
    public function quiz()
    {
        return $this->belongsTo('App\Quiz');
    }
}

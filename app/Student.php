<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 /**
  * Representación de un estudiante.

  @author Maximiliano Fernández <thebluemax13@gmail.com>
  */
class Student extends Model
{
     /**
     * La tabla del modelo.
     *
     * @var string
     */
    protected $table = 'students';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'final_score','professor_id'
    ];
    /**
     * Objeto User de la relación
     *
     * @return User el usuario del profesor.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
     /**
     * el professor de realación profesor usuario
     *
     * @return User el usuario del profesor.
     */
    public function professor()
    {
        return $this->belongsTo('App\Professor');
    }
}

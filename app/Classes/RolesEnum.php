<?php
namespace App\Classes;
/**
 * Enumeración con los códigos de roles
 *
 * @author Maximiliano Fenández <thebluemax13@gmail.com>
 */
class RoleEnum {

    public static $ADMIN = 'A';
    public static $PROFESSOR = 'P';
    public static $USER = 'U';

}

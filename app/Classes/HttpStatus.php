<?php

namespace App\Classes;

/**
 * Enumeración con los códigos de status de las peticiones HTTP
 *
 * @author Maximiliano Fenández <thebluemax13@gmail.com>
 */
class HttpStatus
{
    public static  $OK = 200;
    public static  $CREATED = 201;
    public static  $ACEPTED = 202;
    public static  $NO_CONTENT = 204;
    public static  $BAD_REQUEST = 400;
    public static  $UNAUTHORIZED = 401;
    public static  $FORBIDDEN = 403;
    public static  $NOT_FOUND = 404;
    public static  $NOT_ACCEPTABLE = 406;
    public static  $SERVER_ERROR = 500;
    public static  $NOT_MODIFIED = 304;

}

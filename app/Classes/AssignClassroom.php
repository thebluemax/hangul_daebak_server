<?php

namespace App\Classes;

use Illuminate\Support\Facades\DB;

/**
 *  Clase estática para la asignación de estudiantes.
 */
class AssignClassroom
{
    /**
     * Cuenta la cantidad de alumnos que tiene cada profesor y devuelve el id del profesor que menos
     * estudiantes tiene asignado
     *
     * @return int El id del profesor
     */
    public static function assign()
    {
        $results =  DB::select('SELECT p.id AS professor, count(s.id) AS students FROM professors AS p '.
                                'LEFT JOIN  students AS s ON p.id = s.professor_id GROUP BY p.id;');

        $selectedProfessor = 0;
        $numStudents = 0;
        $flag = true;

        foreach ($results as $result) {
            if ($flag) {//copiamos los resultado del primer profesor
                $selectedProfessor = $result->professor;
                $numStudents = $result->students;
                $flag = false;
            }
            if ($result->students <  $numStudents) {

                $selectedProfessor = $result->professor;
                $numStudents = $result->students;
            }
        }

        return $selectedProfessor;
    }

}

<?php

namespace App\Classes;

use DateTime;
use Illuminate\Support\Facades\DB;

/**
 * Esta clase es un ayudante que guarda los datos de estadísticas a la base de datos
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class StaticsUse
{
    /**
     * Objeto datetime
     *
     * @var DateTime
     */
    private $datetime;

    public static $ACTION_CREATE_TEST="create_test";

    public function __construct()
    {
        $this->datetime = new DateTime();
    }
    /**
     * Not implemented
     *
     * @param [type] $user
     * @return void
     */
    public function saveLogin($user)
    { }
    /**
     * Guarda los cambios de contraseñas
     *
     * @param User $user
     * @param string $who
     * @return void
     */
    public function changePass($user, $who)
    {
        DB::table('password_resets')->insert(
            array(
                'email' => $user->email,
                'asked_for' => $who,//user, admin, lostpass
                'created_at' => $this->datetime->format(' Y-m-d H:i:s')
            )
        );
    }
    /**
     * Guarda las estadísticas de ingreso y fallos de login
     *
     * @param Integer $user_id
     * @param String $action
     * @return void
     */
    public function saveStatics($user_id, $action)
    {
        //TODO if user = 0
        //add a text with more info
        //Delete
         DB::table('users_use')->insert(
            array(
                'user_id' => $user_id,
                'action' => $action,
                'created_at' => $this->datetime->format(' Y-m-d H:i:s')
            )
        );
    }
}

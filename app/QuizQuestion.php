<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
/**
 * Clase pivot entre los exámenes y las preguntas. Esta clase almacena las respuestas del estudiante independientemente del resultado correcto.
 *
 * @author Maximiliano Fernández <thebluemax13@gmail.com>
 */
class QuizQuestion extends Pivot
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * La tabla del modelo.
     *
     * @var string
     */
    protected $table = 'quiz_question';
    //protected $primaryKey = null;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quiz_id', 'question_id', 'answer'
    ];
}

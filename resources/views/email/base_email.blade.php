<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Report diari</title>
    <style>
        body {
            width:600px;
            margin:0px auto;
        }
        .header{
            height:200px;
            background-image: url({{asset('storage/corea_back.png')}});
            font-size: 18pt;
            color: #333;
            border-left:2px solid #2345bc;
            paddin-left:10px;
        }
        .section {
            padding: 20px;
            font-family: sans-serif, monospace;
            font-size: 16pt;
        }
        h3 {
            padding: 5px;
            background-color: cadetblue;
            color: cornsilk;
            width: 100%;
        }
        p>span {
            font-weight: 900;
            color: #aaaaff;
        }
        footer {
            text-align: right;
            padding: 5px;
            background-color:#2345bc;
            color: cornsilk;
            width: 100%;
        }
        em {
            color:brown;
            font-weight: 900;
        }
    </style>
</head>
<body>
    <header class="header">
        <div>
            <img src="{{asset('storage/korean.png')}}" alt="" width="100" srcset="">
        </div>
        <h1>HANGUL DAEBAK</h1>
    </header>
    <section class="section">
        <div class="body-mail">
                @section('body')
                @show
        </div>
    </section>
    <footer>hangul daebak @ 2019</footer>
</body>
</html>

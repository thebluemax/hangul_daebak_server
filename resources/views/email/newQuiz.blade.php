@extends('email.base_email')

@section('body')
        <h3>Hola {{$user_name}}:</h3>
            <p>
                Hi ha un nou Quiz per tu a l'app, el professor {{$professor}} ha fet un quiz per tu. <br>
                "{{$description}}"<br>
                Bona sort.
            </p>
        </div>
@endsection

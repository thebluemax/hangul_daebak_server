@extends('email.base_email')

@section('body')
        <h3>Hola administrador:</h3>
            <p>
                Hi ha un nou participant d'app {{$user_name}}, s'ha assignat a la classe {{$classroom}} del professor {{$professor}}.
            </p>
@endsection

@extends('email.base_email')

@section('body')
        <h3>Hola Administrador:</h3>
            <p>El sistema ha detectat que els següents professors, se han connectat després de dos dies.
            </p>
            <ul>
            @for ($i = 0; $i < count($professors); $i++)
                @if ($date[$i] != 'never')
                    <li> <em>{{ $professors[$i] }}</em> - L'última activitat registrada és de fa {{$date[$i]}} dies</li>
                @else
                    <li> <em>{{ $professors[$i] }}</em> - No s'ha trobat cap activitat del professor.</li>
                @endif
            @endfor
            </ul>
@endsection

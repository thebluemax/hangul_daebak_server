<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>{{$title}}</title>
    <style>
        body {
            font-size: 10pt;
            font-family: sans-serif, monospace;
        }
        .header{
            font-size: 18pt;
            color: #333;
            margin: 0px;
            text-align: center;
        }
        .header>h1, h4{
            border-bottom: 1px solid #333

        }
        #header-1, #header-2 {
            display: inline-block;
            vertical-align: middle;
        }
        #header-1 {
            width:100px;
        }
        .section {
            padding: 5px;
            font-size: 16pt;
            margin: 0px;
        }

        #timestamp {
            font-size: 10pt;
            width: 100%;
            background-color: #333;
            color: white;
            padding: 3px;
        }
        p>span {
            font-weight: 900;
            color: #aaaaff;
        }
        h4 {
            padding:3px;
        }
        h5 {
            padding: 3px;
            text-align: center;
        }
        table {
            width: 80%;
            margin: 10px auto;
            border-collapse: collapse;
            text-align: center;
        }
        th, h5 {
            background-color: #333;
            color:white;
            font-size: 12pt;
        }
        th,  {
            border: 1px solid black;
        }
        td {
            height: 40px;
            vertical-align: middle;
            font-size: 10pt;
            border-bottom: 1px solid black;
        }
        footer {
            margin-top: 20px;
            background-color: #bbb;
            padding: 5px;
            text-align: right;
            position: absolute;
            bottom: 0px;
            width: 100%;
        }
    </style>
</head>
<body>
    <header class="header">
        <div>
            <div id="header-1">
            <img src="{{$urlMailLogo}}" alt="Corea Flag" width="100">
            </div>
            <div id="header-2">
                <h1>HANGUL DAEBAK</h1>
            </div>
        </div>
    </header>
    <section class="section">
        <h2>{{$title}}</h2>
        <p id="timestamp">Data: {{$date}}</p>
        @section('principal')
        @show

    </section>
    <footer>hangul daebak @ 2019</footer>
</body>
</html>

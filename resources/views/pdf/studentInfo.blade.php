@extends('pdf.base_pdf')

@section('principal')

        <div>
            <h4>Estadístiques dels estudiants</h4>
                <table class="egt">
                        <tr>
                          <th>no Estu.</th>
                          <th>no Prof.</th>
                          <th>Estudiants x Professor</th>
                        </tr>
                        <tr>
                          <td>{{$total_students}}</td>
                          <td>{{$total_professors}}</td>
                        <td>{{$promedi_estu_prof}}</td>
                        </tr>
                      </table>
        </div>
        <div>
            <h4>Informe classes</h4>
            <table class="egt">
            <tr>
                    <th>Classe</th>
                    <th>Professor responsable</th>
                    <th>Estudiants assignats</th>
                  </tr>
                  @for ($i = 0; $i < count($classroom); $i++)
                      <tr>
                        <td>{{$classroom[$i]}}</td>
                        <td>{{$professor[$i]}}</td>
                        <td>{{$numStudents[$i]}}</td>
                    </tr>
            @endfor
                </table>
        </div>
        <div>
                <h4>Mètriques acadèmiques</h4>
                <table class="egt">
                        <tr>
                          <th>quantitat total de Punts(Score)</th>
                          <th>Promedi Punts x Usuari</th>
                        </tr>
                        <tr>
                          <td>{{$totalPoints}}</td>
                        <td>{{$promediPoints}}</td>
                        </tr>
                      </table>
        </div>
        <div>
                <h4>Estudiant amb mes punts</h4>
                <table class="egt">
                        <tr>
                          <th>nom</th>
                          <th>Points</th>
                        </tr>
                        <tr>
                          <td>{{$maxStudent}}</td>
                        <td>{{$maxStudentPoints}}</td>
                        </tr>
                      </table>
        </div>
        <div>
                <h4>Quizzes Info</h4>
                <table class="egt">
                        <tr>
                          <th>No Quizzes</th>
                          <th>Quiz x Estudiant</th>
                          <th> Qizzes Resolts</th>
                          <th>Quizzes Sense resoldre</th>
                          <th>Quizzes Aleatoris</th>
                        </tr>
                        <tr>
                          <td>{{$num_gradebooks}}</td>
                          <td>{{$promediGradebooks}}</td>
                          <td>{{$num_solved_gradebooks}}</td>
                        <td>{{$numUnsolvedGradebooks}}</td>
                        <td>{{$numRandomGradebooks}}</td>
                        </tr>
                      </table>
        </div>
@endsection

@extends('pdf.base_pdf')

@section('principal')


<div>
        <h4>Llistat de professors</h4>
            <table class="egt">
                    <tr>
                      <th>Nom</th>
                      <th>Classe</th>
                      <th>Presentació classe</th>
                      <th>Data Inici Activitats</th>
                    </tr>
                    @for ($i = 0; $i < count($professor); $i++)
                    <tr>
                      <td>{{$professor[$i]}}</td>
                      <td>{{$classroom[$i]}}</td>
                    <td>{{$presentation[$i]}}</td>
                    <td>{{$init[$i]}}</td>
                    </tr>
                    @endfor
                  </table>
    </div>
    <div>
            <h4>Darrer accès Professors</h4>
                <table class="egt">
                        <tr>
                          <th>Nom</th>
                          <th>Data Login</th>
                        </tr>
                        @for ($ii = 0; $ii < count($professor); $ii++)
                        <tr>
                          <td>{{$professor[$ii]}}</td>
                        <td>{{$lastLogin[$ii]}}</td>
                        </tr>
                        @endfor
                      </table>
        </div>
        <div>
                <h4>Professor classes</h4>
                    <table class="egt">
                            <tr>
                              <th>nom</th>
                              <th>classe</th>

                              <th>students</th>
                              <th>Total Score</th>
                            </tr>
                            @for ($a = 0; $a < count($professor); $a++)
                            <tr>
                              <td>{{$professor[$a]}}</td>
                            <td>{{$classroom[$a]}}</td>
                            <td>{{$studentClassroom[$a]}}</td>
                            <td>{{$studentsPoints[$a]}}</td>
                            </tr>
                            @endfor
                          </table>
                <h5>Detalls Professors</h5>
                @for ($b = 0; $b < count($professor); $b++)
                <h6>{{$professor[$b]}}</h6>
                <table class="egt">
                    <tr>
                      <th>Quizzes</th>
                      <th>Sense resposta</th>
                      <th>Primer Quiz</th>
                      <th>Últim Quiz</th>
                      <th>Score Del Quizzes Resolts</th>
                    </tr>
                    <tr>
                        <td>{{$totalQuizzes[$b]}}</td>
                        <td>{{$QuizzesToDo[$b]}}</td>
                      <td>{{$firstQuiz[$b]}}</td>
                      <td>{{$lastQuiz[$b]}}</td>
                      <td>{{$studentsPoints[$b]}}</td>
                      </tr>
                </table>
                @endfor
            </div>
@endsection

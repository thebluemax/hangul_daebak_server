<?php

namespace Tests\Feature;

use App\Classes\RoleEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
     $user = array(
        'name' => 'Quim',
        'surname'=> 'Quow',
        'email' => 'quom@gmail.con',
        'password' => 'password',
        'role' => RoleEnum::$USER
    );
    private $user2;
    private $token = "Bearer ";
    private $requestLogingOK= array('email' => 'quom@gmail.con',
    'password' => 'password');
    private $requestLogingBAD= array('email' => 'quom@gmail.con',
    'password' => 'password_Mal');

    public static function setUpBeforeClass(): void
    {
       // Called once just like normal constructor
       // You can create database connections here etc
       echo 'Migration -begin-' . "\n";
      // echo shell_exec('php artisan passport:install');
    //  $this->artisan('migrate:refresh --seed');
    //   $this->artisan('passport:install');
       echo 'Migration -end-' . "\n";


    }
    function setUp(): void
    {
        parent::setUp();

    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function notWillPass()
    {
        $response = $this->json('GET','/api/profile');
        $response->assertUnauthorized();
    }

    public function testLoginBad()
    {

        //  $this->actingAs($user, 'api');
        $response = $this->json('POST','/api/login',$this->requestLogingBAD);

       // $object = json_decode($response->getContent(),true);

        $response->assertStatus(401);

   //     $this->assertArrayHasKey('success', $object);
      //  $this->assertArrayHasKey('token', $object['success']);
        //  $response->assertJson(['success'=>{'token'}]);
        //   $tokenArray = json_decode($response->content());
        $response->assertSeeText('error');
      //   $this->token .= $object['success']['token'];
    }
    public function testLoginOk()
    {

        //  $this->actingAs($user, 'api');
        $response = $this->json('POST','/api/login',$this->requestLogingOK);

        $object = json_decode($response->getContent(),true);

        $response->assertStatus(200);

        $this->assertArrayHasKey('success', $object);
        $this->assertArrayHasKey('token', $object['success']);
        //  $response->assertJson(['success'=>{'token'}]);
        //   $tokenArray = json_decode($response->content());

         $this->token .= $object['success']['token'];

    }
    public function testLogoutWithoutToken()
    {
        $response = $this->json('GET','/api/logout');
        $response->assertStatus(401);

    }

    public function testLogout()
    {
        echo $this->token;
        $response = $this->withHeaders(['Authorization'=>$this->token])
        ->json('GET','/api/logout');
        $response->assertStatus(200);

    }
}
